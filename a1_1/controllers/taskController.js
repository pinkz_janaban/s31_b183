//It will allow us to use the contents of the task.js file in the model folder.

const Task = require("../models/task");

//Get All tasks
module.exports.getAllTasks =() => {
	//The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/postman
	return Task.find({}).then(result => result);
}


//Create a task
module.exports.createTask = (reqBody) => {

	//Create a task object based on the mongoose model "Task"

	let newTask = new Task({
		//Sets the "name" property with the value received from the postman
		name: reqBody.name
	})
		//Using callback function: newUser.save((saveErr, savedTask) => {})
		//Using .then method: newTask.save().then((task, error) => {})
	return newTask.save().then((task, error) => {
		if(error){
			console.log(error)
			return false
		}
		else{
			//Returns the new task object saved in the database back to the postman
			return task
		}
	})
}

//