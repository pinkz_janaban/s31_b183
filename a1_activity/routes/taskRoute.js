const express = require("express");
//The "taskController" allow us to use the funtion defined inside it
const taskController = require("../controllers/taskController");

//Allows access to HTTP Method middlewares that makes it easier to create routes for our application
const router = express.Router();

//Route to get all tasks
router.get("/", (req, res) =>{


	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

//Route to Create a Task
//localhost:3001/tasks it is the same with localhost:3001/task/
router.post("/", (req, res) => {
	//The "createTask" function needs data from the request body, so we need it to supply in the taskController.createTask(argument).
	taskController.createTask(req.body).then(resultFromController => res.send(
		resultFromController));
})



//Route to delete a task
//colon (:) is an identifier that helps create a dynamic route which allow us to supply information in the url
//":id" is a wildcard where you can put the objectID as the value
//Ex: localhost:3001/tasks/:id or localhost:3000/tasks/123456
router.delete("/:id", (req,res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));

})


//Route to update a task
router.patch("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

//retrieving a specific task
router.get('/:id', (req, res) => {

	taskController.getSpecificTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})



//Use "module.exports" to export the router object to be use in the server
module.exports = router;

